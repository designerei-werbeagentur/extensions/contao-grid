# Contao-Grid

## Function & usage

This extension for Contao Open Source CMS allows you to create grid wrapper elements, to create layouts based on css grid layout within the backend. You can add multiple classes to the parent and child elements, based on Tailwind CSS.

## Configuration

This is the default configuration of the options. You can configure these options by adjusting the `config.yml` file.

Note: The rendered class structure is based on Tailwind CSS.

```yml
contao_grid:
  viewports:
    - xs
    - sm
    - md
    - lg
    - xl
  columns:
    - 1
    - 2
    - 3
    - 4
    - 5
    - 6
    - 7
    - 8
    - 9
    - 10
    - 11
    - 12
  rows:
    - 1
    - 2
    - 3
    - 4
    - 5
    - 6
  gaps:
    - 0
    - 1
    - 2
    - 4
    - 8
    - 12
  positioning:
    - align
    - justify
    - place
  directions:
    - start
    - center
    - end
    - stretch
