<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('designerei_contao_grid');

        // Keep compatibility with symfony/config < 4.2
        if (method_exists($treeBuilder, 'getRootNode')) {
           $rootNode = $treeBuilder->getRootNode();
        } else {
           $rootNode = $treeBuilder->root('designerei_contao_grid');
        }

        $rootNode
           ->children()
               ->arrayNode('viewports')
                   ->scalarPrototype()->end()
                   ->defaultValue(['xs', 'sm', 'md', 'lg', 'xl'])
               ->end()
               ->arrayNode('columns')
                   ->integerPrototype()->end()
                   ->defaultValue(range(1, 12))
               ->end()
               ->arrayNode('rows')
                   ->integerPrototype()->end()
                   ->defaultValue(range(1, 6))
               ->end()
               ->arrayNode('gaps')
                   ->integerPrototype()->end()
                   ->defaultValue([0, 1, 2, 4, 8, 12])
               ->end()
               ->arrayNode('positioning')
                   ->scalarPrototype()->end()
                   ->defaultValue(['align', 'justify', 'place'])
               ->end()
               ->arrayNode('directions')
                   ->scalarPrototype()->end()
                   ->defaultValue(['start', 'center', 'end', 'stretch'])
               ->end()
           ->end()
        ;

        return $treeBuilder;
    }
}
