<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle;

use Symfony\Component\Filesystem\Filesystem;

final class GridClasses
{

    /** @var string[] */
    private $viewports;

    /** @var int[] **/
    private $columns;

    /** @var int[] **/
    private $rows;

    /** @var int[] **/
    private $gaps;

    /** @var string[] */
    private $positioning;

    /** @var string[] */
    private $directions;

    public function __construct(
        array $viewports,
        array $columns,
        array $rows,
        array $gaps,
        array $positioning,
        array $directions
    ) {
        $this->viewports   = $viewports;
        $this->columns     = $columns;
        $this->rows        = $rows;
        $this->gaps        = $gaps;
        $this->positioning = $positioning;
        $this->directions  = $directions;
    }

    public function getViewports(): array
    {
        return $this->viewports;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function getRows(): array
    {
        return $this->rows;
    }

    public function getGaps(): array
    {
        return $this->gaps;
    }

    public function getPositioning(): array
    {
        return $this->positioning;
    }

    public function getDirections(): array
    {
        return $this->directions;
    }

    // Grid Template Columns
    public function getGridTemplateColumnsOptions(): array
    {
        $options = [];
        $prefix = 'grid-cols';

        foreach ($this->getColumns() as $column) {
            $options[$prefix][] = implode([$prefix, '-', $column]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getColumns() as $column) {
                $options[$viewport.':'.$prefix][] = implode([$viewport, ':', $prefix, '-', $column]);
            }
        }

        return $options;
    }

    // Grid Template Rows
    public function getGridTemplateRowsOptions(): array
    {
        $options = [];

        foreach ($this->getRows() as $rows) {
            $options['grid-rows'][] = implode(['grid-rows', '-', $rows]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getRows() as $rows) {
                $options[$viewport.':'.'grid-rows'][] = implode([$viewport, ':', 'grid-rows', '-', $rows]);
            }
        }

        return $options;
    }

    // Grid Gap
    public function getGridGapOptions(): array
    {
        $options = [];

        $axes = ['', 'x','y'];

        $gaps = $this->getGaps();
        array_unshift($gaps, 'px');

        foreach ($gaps as $gap) {
            foreach ($axes as $axis) {
                if(!$axis) {
                    $options['gap'][] = implode(['gap','-', $gap]);
                } else {
                    $options['gap'][] = implode(['gap', '-', $axis ,'-', $gap]);
                }
            }
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($gaps as $gap) {
                foreach ($axes as $axis) {
                    if(!$axis) {
                        $options[$viewport.':'.'gap'][] = implode([$viewport, ':', 'gap','-', $gap]);
                    } else {
                        $options[$viewport.':'.'gap'][] = implode([$viewport, ':', 'gap', '-', $axis ,'-', $gap]);
                    }
                }
            }
        }

        return $options;
    }

    // Grid Columns
    public function getGridColumnsOptions(): array
    {
        $options = [];

        // col
        $options['col'][] = 'col-auto';

        foreach ($this->getViewports() as $viewport) {
            $options[$viewport.':'.'col'][] = $viewport . ':' . 'col-auto';
        }

        // col-span
        $columns = $this->getColumns();
        array_push($columns, 'full');
        foreach ($columns as $column) {
            $options['col-span'][] = implode(['col-span', '-', $column]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($columns as $column) {
                $options[$viewport.':'.'col-span'][] = implode([$viewport . ':' . 'col-span', '-', $column]);
            }
        }

        // add additional item
        $columns = $this->getColumns();
        $lastItem = end($columns);
        $additionalItem = $lastItem + 1;
        array_push($columns, $additionalItem, 'auto');

        // col-start
        foreach ($columns as $column) {
            $options['col-start'][] = implode(['col-start', '-', $column]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($columns as $column) {
                $options[$viewport.':'.'col-start'][] = implode([$viewport . ':' . 'col-start', '-', $column]);
            }
        }

        // col-end
        foreach ($columns as $column) {
            $options['col-end'][] = implode(['col-end', '-', $column]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($columns as $column) {
                $options[$viewport.':'.'col-end'][] = implode([$viewport . ':' . 'col-end', '-', $column]);
            }
        }

        return $options;
    }

    // Grid Rows
    public function getGridRowsOptions(): array
    {
        $options = [];


        // row
        $options['row'][] = 'row-auto';

        foreach ($this->getViewports() as $viewport) {
            $options[$viewport.':'.'row'][] = $viewport . ':' . 'row-auto';
        }

        // row-span
        $rows = $this->getRows();
        array_push($rows, 'full');
        foreach ($rows as $row) {
            $options['row-span'][] = implode(['row-span', '-', $row]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($rows as $row) {
                $options[$viewport.':'.'row-span'][] = implode([$viewport . ':' . 'row-span', '-', $row]);
            }
        }

        // add additional item
        $rows = $this->getRows();
        $lastItem = end($rows);
        $additionalItem = $lastItem + 1;
        array_push($rows, $additionalItem, 'auto');

        // row-start
        foreach ($rows as $row) {
            $options['row-start'][] = implode(['row-start', '-', $row]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($rows as $row) {
                $options[$viewport.':'.'row-start'][] = implode([$viewport . ':' . 'row-start', '-', $row]);
            }
        }

        // row-end
        foreach ($rows as $row) {
            $options['row-end'][] = implode(['row-end', '-', $row]);
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($rows as $row) {
                $options[$viewport.':'.'row-end'][] = implode([$viewport . ':' . 'row-end', '-', $row]);
            }
        }

        return $options;
    }

    // Alignment (parent)
    public function getGridAlignmentOptions(): array
    {
        $options = [];

        foreach ($this->getPositioning() as $position) {
            if($position == 'align') {
                $directions = $this->getDirections();
                array_push($directions, 'baseline');
                foreach ($directions as $direction) {
                  $options[$position.'-items'][] = implode(['items-'.$direction]);
                }
            } else {
                foreach ($this->getDirections() as $direction) {
                  $options[$position.'-items'][] = implode([$position.'-items-'.$direction]);
                }
            }
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getPositioning() as $position) {
                if($position == 'align') {
                    $directions = $this->getDirections();
                    array_push($directions, 'baseline');
                    foreach ($directions as $direction) {
                      $options[$viewport.':'.$position.'-items'][] = implode([$viewport . ':' . 'items-'.$direction]);
                    }
                } else {
                    foreach ($this->getDirections() as $direction) {
                      $options[$viewport.':'.$position.'-items'][] = implode([$viewport . ':' . $position.'-items-'.$direction]);
                    }
                }
            }
        }

        return $options;
    }

    // Alignment (child)
    public function getGridItemAlignmentOptions(): array
    {
        $options = [];

        $directions = $this->getDirections();
        array_unshift($directions, 'auto');

        foreach ($this->getPositioning() as $position) {
            if($position == 'align') {

                foreach ($directions as $direction) {
                  $options[$position.'-self'][] = implode(['self-'.$direction]);
                }
            } else {
                foreach ($directions as $direction) {
                  $options[$position.'-self'][] = implode([$position.'-self-'.$direction]);
                }
            }
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getPositioning() as $position) {
                if($position == 'align') {
                    $directions = $this->getDirections();
                    foreach ($directions as $direction) {
                      $options[$viewport.':'.$position.'-self'][] = implode([$viewport . ':' . 'self-'.$direction]);
                    }
                } else {
                    foreach ($directions as $direction) {
                      $options[$viewport.':'.$position.'-self'][] = implode([$viewport . ':' . $position.'-self-'.$direction]);
                    }
                }
            }
        }

        return $options;
    }

    // generate safelist
    public function generateSafelist(): string
    {
        $safelist = '';
        $prefix = 'grid-cols';

        // Grid Template Columns
        foreach ($this->getColumns() as $column) {
            $safelist .= ' ' . $prefix . '-' . $column;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getColumns() as $column) {
                $safelist .= ' ' . $viewport . ':' . $prefix . '-' . $column;
            }
        }

        // Grid Template Rows
        foreach ($this->getRows() as $rows) {
            $safelist .= ' ' . 'grid-rows' . '-' . $rows;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getRows() as $rows) {
                $safelist .= ' ' . $viewport . ':' . 'grid-rows' . '-' . $rows;
            }
        }

        // Grid Gap
        $axes = ['', 'x','y'];

        $gaps = $this->getGaps();
        array_unshift($gaps, 'px');

        foreach ($gaps as $gap) {
            foreach ($axes as $axis) {
                if(!$axis) {
                    $safelist .= ' ' . 'gap' . '-' . $gap;
                } else {
                    $safelist .= ' ' . 'gap' . '-' . $axis . '-' . $gap;
                }
            }
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($gaps as $gap) {
                foreach ($axes as $axis) {
                    if(!$axis) {
                        $safelist .= ' ' . $viewport . ':' . 'gap' . '-' . $gap;
                    } else {
                        $safelist .= ' ' . $viewport . ':' . 'gap' . '-' . $axis . '-' . $gap;
                    }
                }
            }
        }

        // Grid Columns
        $safelist .= ' ' . 'col-auto';

        foreach ($this->getViewports() as $viewport) {
            $safelist .= ' ' . $viewport . ':' . 'col-auto';
        }

        // col-span
        $columns = $this->getColumns();
        array_push($columns, 'full');
        foreach ($columns as $column) {
            $safelist .= ' ' . 'col-span' . '-' . $column;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($columns as $column) {
                $safelist .= ' ' . $viewport . ':' . 'col-span' . '-' . $column;
            }
        }

        // add additional item
        $columns = $this->getColumns();
        $lastItem = end($columns);
        $additionalItem = $lastItem + 1;
        array_push($columns, $additionalItem, 'auto');

        // col-start
        foreach ($columns as $column) {
            $safelist .= ' ' . 'col-start' . '-' . $column;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($columns as $column) {
                $safelist .= ' ' . $viewport . ':' . 'col-start' . '-' . $column;
            }
        }

        // col-end
        foreach ($columns as $column) {
            $safelist .= ' ' . 'col-end' . '-' . $column;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($columns as $column) {
                $safelist .= ' ' . $viewport . ':' . 'col-end' . '-' . $column;
            }
        }

        // Grid Rows
        // row
        $safelist .= ' ' . 'row-auto';

        foreach ($this->getViewports() as $viewport) {
            $safelist .= ' ' . $viewport . ':' . 'row-auto';
        }

        // row-span
        $rows = $this->getRows();
        array_push($rows, 'full');
        foreach ($rows as $row) {
            $safelist .= ' ' . 'row-span' . '-' . $row;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($rows as $row) {
                $safelist .= ' ' . $viewport . ':' . 'row-span' . '-' . $row;
            }
        }

        // add additional item
        $rows = $this->getRows();
        $lastItem = end($rows);
        $additionalItem = $lastItem + 1;
        array_push($rows, $additionalItem, 'auto');

        // row-start
        foreach ($rows as $row) {
            $safelist .= ' ' . 'row-start' . '-' . $row;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($rows as $row) {
                $safelist .= ' ' . $viewport . ':' . 'row-start' . '-' . $row;
            }
        }

        // row-end
        foreach ($rows as $row) {
            $safelist .= ' ' . 'row-end' . '-' . $row;
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($rows as $row) {
                $safelist .= ' ' . $viewport . ':' . 'row-end' . '-' . $row;
            }
        }

        // Alignment (parent)
        foreach ($this->getPositioning() as $position) {
            if($position == 'align') {
                $directions = $this->getDirections();
                array_push($directions, 'baseline');
                foreach ($directions as $direction) {
                    $safelist .= ' ' . 'items-' . $direction;
                }
            } else {
                foreach ($this->getDirections() as $direction) {
                    $safelist .= ' ' . $position . '-items-' . $direction;
                }
            }
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getPositioning() as $position) {
                if($position == 'align') {
                    $directions = $this->getDirections();
                    array_push($directions, 'baseline');
                    foreach ($directions as $direction) {
                        $safelist .= ' ' . $viewport . ':' . 'items-' . $direction;
                    }
                } else {
                    foreach ($this->getDirections() as $direction) {
                        $safelist .= ' ' . $viewport . ':' . $position . '-items-' . $direction;
                    }
                }
            }
        }

        // Alignment (child)
        $directions = $this->getDirections();
        array_unshift($directions, 'auto');

        foreach ($this->getPositioning() as $position) {
            if($position == 'align') {

                foreach ($directions as $direction) {
                    $safelist .= ' ' . 'self-' . $direction;
                }
            } else {
                foreach ($directions as $direction) {
                    $safelist .= ' ' . $position . '-self-' . $direction;
                }
            }
        }

        foreach ($this->getViewports() as $viewport) {
            foreach ($this->getPositioning() as $position) {
                if($position == 'align') {
                    $directions = $this->getDirections();
                    foreach ($directions as $direction) {
                        $safelist .= ' ' . $viewport . ':' . 'self-' . $direction;
                    }
                } else {
                    foreach ($directions as $direction) {
                        $safelist .= ' ' . $viewport . ':' . $position . '-self-' . $direction;
                    }
                }
            }
        }

        $safelist = ltrim($safelist);

        // create safelist file
        $cur_dir = \dirname(__DIR__);
        $public_dir = $cur_dir.'/src/Resources/public';

        $fs = new Filesystem();
        $fs->mkdir($public_dir);
        $fs->dumpFile($public_dir.'/safelist.txt', $safelist);

        return $safelist;
    }
}
