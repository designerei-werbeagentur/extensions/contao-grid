<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle;

final class ContainerClasses
{
    // Container Max Width
    public function getContainerMaxWidthOptions(): array
    {
        $options = [];

        $sizes = [
            'xs',
            'sm',
            'md',
            'lg',
            'xl',
            'screen-sm',
            'screen-md',
            'screen-lg',
            'screen-xl'
        ];

        foreach ($sizes as $size) {
            $options[] = implode(['max-w'.'-'.$size]);
        };

        return $options;
    }
}
