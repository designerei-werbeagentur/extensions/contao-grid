<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Contao\BackendTemplate;
use Contao\StringUtil;

/**
 * @ContentElement("containerStart",
 *   category="container",
 * )
 */
class ContainerStart extends AbstractContentElementController
{
    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {
        // add gridColumns to class
        if($template->gridColumns) {
            $gridColumns = StringUtil::deserialize($template->gridColumns);
            foreach($gridColumns as $class) {
                $template->class .= ' ' . $class;
            }
        }

        // add gridRows to class
        if($template->gridRows) {
            $gridRows = StringUtil::deserialize($template->gridRows);
            foreach($gridRows as $class) {
                $template->class .= ' ' . $class;
            }
        }

        // add containerMaxWidth to class
        if($template->containerMaxWidth) {
            $containerMaxWidth = $template->containerMaxWidth;
            $template->class .= ' ' . $containerMaxWidth;
        }

        // add containerCenter to class
        if($template->containerCenter) {
            $containerCenter = $template->containerCenter;
            $template->class .= ' ' . 'mx-auto';
        }

        $type   = $template->type;
        $class  = $template->class;
        $cssID  = $template->cssID;

        if (TL_MODE === 'BE') {
            $template           = new BackendTemplate('be_wildcard');
            $template->wildcard = implode(array(
              "&lt;",
              'div',
              ($cssID) ? $cssID : '',
              ' class="' . $class . '"',
              "&gt;"
            ));
        }

        return $template->getResponse();

        //
        // twig workflow
        //

        // return $this->render(
        //   '@ContaoGrid/ce_containerStart.html.twig',
        //   $model->row()
        // );
    }
}
