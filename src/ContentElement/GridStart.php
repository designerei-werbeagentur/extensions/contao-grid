<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Contao\BackendTemplate;
use Contao\StringUtil;

/**
 * @ContentElement("gridStart",
 *   category="grid",
 * )
 */
class GridStart extends AbstractContentElementController
{
    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {
        // add class grid
        $template->class .= ' grid';

        // add gridTemplateColumns to class
        if($template->gridTemplateColumns) {
            $gridTemplateColumns = StringUtil::deserialize($template->gridTemplateColumns);
            foreach($gridTemplateColumns as $value) {
                $template->class .= ' ' . $value;
            }
        }

        // add gridTemplateRows to class
        if($template->gridTemplateRows) {
            $gridTemplateRows = StringUtil::deserialize($template->gridTemplateRows);
            foreach($gridTemplateRows as $value) {
                $template->class .= ' ' . $value;
            }
        }

        // add gridGap to class
        if($template->gridGap) {
            $gridGap = StringUtil::deserialize($template->gridGap);
            foreach($gridGap as $value) {
                $template->class .= ' ' . $value;
            }
        }

        // add gridClass to class
        if($template->gridClass) {
            $gridClass = $template->gridClass;
            $template->class .= ' ' . $gridClass;
        }

        // add containerMaxWidth to class
        if($template->containerMaxWidth) {
            $containerMaxWidth = $template->containerMaxWidth;
            $template->class .= ' ' . $containerMaxWidth;
        }

        // add containerCenter to class
        if($template->containerCenter) {
            $containerCenter = $template->containerCenter;
            $template->class .= ' ' . 'mx-auto';
        }

        // add gridAlignment to class
        if($template->gridAlignment) {
            $gridAlignment = StringUtil::deserialize($template->gridAlignment);
            foreach($gridAlignment as $value) {
                $template->class .= ' ' . $value;
            }
        }

        $type   = $template->type;
        $class  = $template->class;
        $cssID  = $template->cssID;

        if (TL_MODE === 'BE') {
            $template           = new BackendTemplate('be_wildcard');
            $template->wildcard = implode(array(
                "&lt;",
                'div',
                ($cssID) ? $cssID : '',
                ' class="' . $class . '"',
                "&gt;"
            ));
        }

        return $template->getResponse();

        //
        // twig workflow
        //

        // return $this->render(
        //   '@ContaoGrid/ce_gridEnd.html.twig',
        //   $model->row()
        // );
    }
}
