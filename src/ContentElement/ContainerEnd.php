<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Contao\BackendTemplate;

/**
 * @ContentElement("containerEnd",
 *   category="container",
 * )
 */
class ContainerEnd extends AbstractContentElementController
{
    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {
        if (TL_MODE === 'BE') {
            $template           = new BackendTemplate('be_wildcard');
            $template->wildcard = implode(array(
              "&lt;/",
              'div',
              "&gt;"
            ));
        };

        return $template->getResponse();

        //
        // twig workflow
        //

        // return $this->render(
        //   '@ContaoGrid/ce_containerEnd.html.twig',
        //   $model->row()
        // );
    }
}
