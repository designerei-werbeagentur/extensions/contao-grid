<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoGridBundle extends Bundle
{
}
