<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\EventListener\DataContainer;

use designerei\ContaoGridBundle\ContainerClasses;

final class ContainerMaxWidthOptionsListener
{
    private $containerClasses;

    public function __construct(ContainerClasses $containerClasses)
    {
        $this->containerClasses = $containerClasses;
    }

    public function onOptionsCallback(): array
    {
        return $this->containerClasses->getContainerMaxWidthOptions();
    }
}
