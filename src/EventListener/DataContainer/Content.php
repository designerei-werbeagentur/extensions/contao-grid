<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\EventListener\DataContainer;

use Contao\ContentModel;
use Contao\Database;
use InvalidArgumentException;

final class Content
{
    public function onsubmitCallback(\DataContainer $dc)
    {
        if (!\in_array($dc->activeRecord->type, ['containerStart', 'gridStart', 'flexStart'], true)) {
            return;
        }

        if ('auto' !== \Contao\Input::post('SUBMIT_TYPE') && $this->siblingStopElmentIsMissing(
                $dc->activeRecord->pid,
                $dc->activeRecord->ptable,
                $dc->activeRecord->sorting,
                str_replace(array('Start', 'End'), '', $dc->activeRecord->type)
            )) {
            $data = $dc->activeRecord->row();
            unset($data['id']);
            $data['type']    = str_replace('Start', 'End', $dc->activeRecord->type);
            ++$data['sorting'];

            $newElement = new ContentModel();
            $newElement->setRow($data);
            $newElement->save();
        }
    }

    private function siblingStopElmentIsMissing($pid, $ptable, $sorting, $typeStart)
    {
        if (!\in_array($typeStart, ['container', 'grid', 'flex'], true)) {
            throw new InvalidArgumentException('Argument $typeStart must be either "container", "grid" or "flex"');
        }

        $statement = Database::getInstance()
            ->prepare(
                'SELECT * FROM tl_content WHERE pid=? AND ptable=? AND sorting>? AND type IN("'.$typeStart.'Start", "'.$typeStart.'End") ORDER BY sorting'
            )
            ->limit(1)
            ->execute($pid, $ptable, $sorting);

        if (false === $row = $statement->fetchAssoc()) {
            return true;
        }

        if ($typeStart.'End' !== $row['type']) {
            return true;
        }

        return false;
    }
}
