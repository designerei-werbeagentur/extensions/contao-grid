<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\EventListener\DataContainer;

use Contao\DataContainer;

final class RegisterFieldsInPaletteListener
{
    public function onLoadContentCallback(DataContainer $dataContainer): void
    {
        foreach ($GLOBALS['TL_DCA']['tl_content']['palettes'] as $k => $palette) {
            if (!\is_array($palette) && false == strpos($palette, '{grid_legend')) {
                $GLOBALS['TL_DCA']['tl_content']['palettes'][$k] = str_replace(
                    '{expert_legend',
                    '{grid_legend:hide},gridColumns,gridRows,gridItemAlignment;{expert_legend',
                    $GLOBALS['TL_DCA']['tl_content']['palettes'][$k]
                );
            }
        }
    }
}
