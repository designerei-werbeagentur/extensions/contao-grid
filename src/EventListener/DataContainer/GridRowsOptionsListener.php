<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\EventListener\DataContainer;

use designerei\ContaoGridBundle\GridClasses;

final class GridRowsOptionsListener
{
    private $gridClasses;

    public function __construct(GridClasses $gridClasses)
    {
        $this->gridClasses = $gridClasses;
    }

    public function onOptionsCallback(): array
    {
        return $this->gridClasses->getGridRowsOptions();
    }
}
