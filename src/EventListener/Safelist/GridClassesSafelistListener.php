<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\EventListener\Safelist;

use designerei\ContaoGridBundle\GridClasses;
use Contao\CoreBundle\ServiceAnnotation\Hook;

/**
 * @Hook("initializeSystem")
 */
class GridClassesSafelistListener
{
    private $gridClasses;

    public function __construct(GridClasses $gridClasses)
    {
        $this->gridClasses = $gridClasses;
    }

    public function __invoke(): void
    {
        $this->gridClasses->generateSafelist();
    }
}
