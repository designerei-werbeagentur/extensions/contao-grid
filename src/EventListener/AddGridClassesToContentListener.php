<?php

declare(strict_types=1);

namespace designerei\ContaoGridBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Contao\StringUtil;

/**
 * @Hook("parseTemplate")
 */
class AddGridClassesToContentListener
{
    public function __invoke(Template $template): void
    {
        if($template->gridColumns) {
            $gridColumns = StringUtil::deserialize($template->gridColumns);
            foreach ($gridColumns as $class) {
                $template->class .= ' ' . $class;
            }
        }

        if($template->gridRows) {
            $gridRows = StringUtil::deserialize($template->gridRows);
            foreach ($gridRows as $class) {
                $template->class .= ' ' . $class;
            }
        }

        if($template->gridItemAlignment) {
            $gridItemAlignment = StringUtil::deserialize($template->gridItemAlignment);
            foreach ($gridItemAlignment as $class) {
                $template->class .= ' ' . $class;
            }
        }
    }
}
