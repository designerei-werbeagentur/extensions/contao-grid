<?php

declare(strict_types=1);

$GLOBALS['TL_WRAPPERS']['start'][] = 'containerStart';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'containerEnd';
$GLOBALS['TL_WRAPPERS']['start'][] = 'gridStart';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'gridEnd';
