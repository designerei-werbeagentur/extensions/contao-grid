<?php

declare(strict_types=1);

use designerei\ContaoGridBundle\EventListener\DataContainer\Content;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridTemplateColumnsOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridTemplateRowsOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridGapOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridColumnsOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridRowsOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridAlignmentOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\GridItemAlignmentOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\ContainerMaxWidthOptionsListener;
use designerei\ContaoGridBundle\EventListener\DataContainer\RegisterFieldsInPaletteListener;

// Config
$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = [RegisterFieldsInPaletteListener::class, 'onLoadContentCallback'];
$GLOBALS['TL_DCA']['tl_content']['config']['onsubmit_callback'][] = [Content::class, 'onsubmitCallback'];

// Palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['containerStart'] = '{type_legend},type;{container_legend:hide},containerMaxWidth,containerCenter;{expert_legend:hide},cssID;';
$GLOBALS['TL_DCA']['tl_content']['palettes']['containerEnd']   = '{type_legend},type;';
$GLOBALS['TL_DCA']['tl_content']['palettes']['gridStart']      = '{type_legend},type;{grid_legend},gridTemplateColumns,gridTemplateRows,gridGap,gridAlignment,gridClass;{container_legend:hide},containerMaxWidth,containerCenter;{expert_legend:hide},cssID;';
$GLOBALS['TL_DCA']['tl_content']['palettes']['gridEnd']        = '{type_legend},type;';

// Fields
$GLOBALS['TL_DCA']['tl_content']['fields']['gridTemplateColumns'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [GridTemplateColumnsOptionsListener::class, 'onOptionsCallback'],
    'eval'             => [
        'tl_class'     => 'w50 w50h autoheight',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridTemplateRows'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [GridTemplateRowsOptionsListener::class, 'onOptionsCallback'],
    'eval'             => [
        'tl_class'     => 'w50 w50h autoheight',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridGap'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [GridGapOptionsListener::class, 'onOptionsCallback'],
    'eval'             => [
        'tl_class'     => 'w50 w50h autoheight',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridClass'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridColumns'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [gridColumnsOptionsListener::class, 'onOptionsCallback'],
    'options'          => ['col-1', 'col-2'],
    'eval'             => [
        'tl_class'     => 'w50 w50h autoheight',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridRows'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [gridRowsOptionsListener::class, 'onOptionsCallback'],
    'options'          => ['row-1', 'row-2'],
    'eval'             => [
        'tl_class'     => 'w50',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridAlignment'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [GridAlignmentOptionsListener::class, 'onOptionsCallback'],
    'options'          => ['row-1', 'row-2'],
    'eval'             => [
        'tl_class'     => 'w50',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['gridItemAlignment'] = [
    'exclude'          => true,
    'inputType'        => 'select',
    'options_callback' => [GridItemAlignmentOptionsListener::class, 'onOptionsCallback'],
    'options'          => ['row-1', 'row-2'],
    'eval'             => [
        'tl_class'     => 'w50',
        'multiple'     => true,
        'size'         => '10',
        'chosen'       => true,
        'mandatory'    => false
    ],
    'sql'              => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['containerMaxWidth'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options_callback'        => [containerMaxWidthOptionsListener::class, 'onOptionsCallback'],
    'eval'                    => array('tl_class'=>'w50', 'includeBlankOption'=>true),
    'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['containerCenter'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
);
